###################################
####### Dynamic Listing AEL001 ####
###################################

####### Reading mulitple libraries in one go

Libraries <- c("haven","Hmisc", "officer","magrittr") 

lapply (Libraries,library, character.only = TRUE)

#########################################

######## Creating function 

AEL001 <- function( data, Variable_1, Variable_2, Variable_3, Variable_4,
                    Variable_5, Variable_6, Variable_7, Variable_8, Variable_9,
                    Variable_10, Variable_11, Variable_12, Variable_13, Variable_14, Variable_15, Variable_16, 
                    Variable_17,Variable_18, Variable_19, Variable_20, Variable_21, Variable_22, Variable_23, Variable_24, Variable_25,Variable_26,
                    Title_1, Title_2, Title_3, Title_4,
                    Footnotes_1, Footnotes_2, Footnotes_3, Footnotes_4,
                    Footnotes_5,Footnotes_6,Footnotes_7) {
    data <- data
  
    data <- subset (data, select = c( Variable_1, Variable_2, Variable_3, Variable_4,
                                    Variable_5, Variable_6, Variable_7, Variable_8, Variable_9,
                                    Variable_10, Variable_11, Variable_12, Variable_13, Variable_14))
  
    data [[Variable_15]] <- paste (data[[Variable_4]], data[[Variable_5]], data[[Variable_6]], sep ="\n")
  
    data [[Variable_7]] <- as.Date(  data [[Variable_7]], format = "%Y-%m-%d")
    data [[Variable_8]] <- as.Date(  data [[Variable_8]], format = "%Y-%m-%d")
  
    data[[Variable_16]] <- data[[Variable_8]] - data[[Variable_7]]
  
    data <- subset (data, data[[Variable_14]] %in% "Y")
  
    data[[Variable_17]] <- data[[Variable_1]]
    data[[Variable_18]] <- data[[Variable_2]]
    data[[Variable_19]] <- data[[Variable_3]]
    data[[Variable_20]] <- data[[Variable_7]]
    data[[Variable_21]] <- data[[Variable_8]]
    data[[Variable_22]] <- data[[Variable_9]]
    data[[Variable_23]] <- data[[Variable_10]]
    data[[Variable_24]] <- data[[Variable_11]]
    data[[Variable_25]] <- data[[Variable_12]]
    data[[Variable_26]] <- data[[Variable_13]]
  
    data <- subset (data, select = c(  Variable_17, Variable_18, Variable_19, Variable_15, Variable_20, Variable_21, 
                                     Variable_16, Variable_22,
                                     Variable_23, Variable_24, Variable_25, Variable_26))
  
  
    my_doc <- read_docx() ## creating blank document
    my_doc <- my_doc %>% 
    body_add_par(Title_1, style = "centered") %>% #Title 1
    body_add_par(Title_2, style = "centered") %>% #Title 2
    body_add_par(Title_3, style = "Normal") %>% #Title 3
    body_add_par(Title_4, style = "Normal") %>% #Title 4
    body_add_par("", style = "centered") %>% #Title 5
    body_add_par("", style = "centered") %>% #Title 6
    body_add_table(data, style = "Normal Table") %>%  
    cursor_forward() %>%
    body_add_par("", style = "toc 1") %>%
    body_add_par("", style = "toc 1") %>%
    body_add_par(" ", style = "toc 1") %>%
    
    body_add_par(Footnotes_1,style = "toc 1") %>%
    body_add_par(Footnotes_2,style = "toc 1") %>%
    body_add_par(Footnotes_3,style = "toc 1") %>%
    body_add_par(Footnotes_4,style = "toc 1") %>%
    body_add_par(Footnotes_5,style = "toc 1") %>%
    body_add_par(Footnotes_6,style = "toc 1") %>%
    body_add_par(Footnotes_7,style = "toc 1") %>%
    body_add_par(paste("TIME STAMP: ",Sys.time()), style = "toc 1") %>%
    
    body_end_section(landscape = TRUE)
  
    print(my_doc, target = "AEL001.docx") 
}

AEL001 ( data = read_sas("C:\\Users\\MAHESAR1\\Desktop\\DEDRR\\Dynamic reporting\\adae.sas7bdat"),
         Variable_1 = 'CCS', 
         Variable_2 = 'ASR', 
         Variable_3 = 'AESER',
         Variable_4 = 'AETERM',
         Variable_5 = 'AEDECOD', 
         Variable_6 = 'AEBODSYS',
         Variable_7 = 'AESTDTC', 
         Variable_8 = 'AEENDTC', 
         Variable_9 = 'AETOXGR',
         Variable_10 = 'AERELN', 
         Variable_11 = 'AEACNOTN', 
         Variable_12 = 'AEACNN', 
         Variable_13 = 'AEOUTN', 
         Variable_14 = 'SAFFL',
         Variable_15 = paste('Reported term','\n',' Preferred term','\n','System organ class', collapse = ""),
         Variable_16 = paste('Dur','\n','ation','\n','(days)',collapse=""),
         Variable_17 = paste("Country/","\n","Subject","\n","Identifier",collapse =""),
         Variable_18 = paste ('Age/','\n','Sex','\n','Race',collapse = ""),
         Variable_19 = paste ('Serious','\n','event',collapse = ""),
         Variable_20 = paste ('Start','\n','date',collapse = ""),
         Variable_21 = paste ('End','\n','date',collapse = ""),
         Variable_22 = paste ('Toxi','\n','city','\n','Grade',collapse = ""),
         Variable_23 = paste ('Cau','\n','sal','\n','ity',collapse = ""),
         Variable_24 = paste ('Action','\n','taken','\n','with','\n','med.',collapse = ""),
                             Variable_25 = paste ('Med','\n','or','\n','ther','\n','taken.',collapse = ""),
                             Variable_26 = paste ('Out','\n','Come',collapse = ""),
                             Title_1 = "Adverse events", 
                             Title_2 = "Safety set", 
                             Title_3 = "Subset: <subset id name>", 
                             Title_4 = 'Actual Treatment: XXXXXXXXXX',
                             Footnotes_1 = '- Day is relative to the reference start date.  Missing dates are based on imputed dates.',
                             Footnotes_2 = '- Severity: MILD=Mild, MOD=Moderate, SEV=Severe.',
                             Footnotes_3 = '- Relationship to study treatment (causality): 1=No, 2=Yes, 3= Yes, investigational treatment, 4= Yes, other study treatment (non-investigational), 
                              5= Yes, both and/or indistinguishable.',
                             Footnotes_4 = '- Action taken: 1=None, 2=Dose adjusted, 3=Temporarily interrupted, 4=Permanently discontinued, 997=Unknown, 999=NA.',
                             Footnotes_5 = '- Outcome: 1=Not recovered/not resolved, 2=recovered/resolved 3=recovering/resolving, 4=recovered/resolved with sequelae, 5=Fatal, 997=Unknown.',
                             Footnotes_6 = '- Medication or therapies taken: 1=No concomitant medication or non-drug therapy, 10=Concomitant medication or non-drug therapy.',
                             Footnotes_7 = '- Time to AE = Time of AE onset minus time of the most recent dose.')