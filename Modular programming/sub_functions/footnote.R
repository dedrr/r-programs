library(xlsx)
library(dplyr)

footnotes = function (program_id) {
  footnote_data= read.xlsx("/home/georganh/georganh/Dynamic_Reporting/param_labels.xlsx", 
                        sheetIndex =3, stringsAsFactors=FALSE)
  return(footnote_data[footnote_data$Program_Name == program_id,])
}

