#ogb=read_excel(path, sheet = 2, col_names = TRUE, col_types = NULL, na = "", skip = 0)

#setwd("/view/georganh_view/vob/REPCENTRAL/TFL/Configuration/GenMed")

#setwd("/view/georganh_view/vob/CAVA/CONC001A2301/") #csr_1/") #analysis_data/")

#dir()
library(readxl)
library(haven)
library(officer)
library(dplyr)

#--> can break this as study if the folders follow same structure
dataset_path = "/view/georganh_view/vob/CAVA/CONC001A2301/csr_1/"
dataset_type = "analysis_data"
dataset_name="adae"
program_id = "AEL003"
std_add_whr_clause = "AEREL == 'YES', !is.na(AETERM)"
std_sort_column = "CCS, ASTDY, AENDY"
std_concat = "AERPT=AETERM AEDECOD AEBODSYS "
std_cols="CCS ASTDY AENDY AERPT"
std_rename="AERPT=Reported Term '\n' End date"

std_onc_dev_listing = function(program_id,
                               dataset_path,
                               dataset_type,   #analsyis or derived - make it user friendly
                               dataset_name,   #adae, adsl    
                               output_number,
                               std_popid,
                               std_sort_column,
                               std_header,
                               std_footer,
                               std_cols) {
  
  #USE DATASET PATH AND DATASET TYPE PARAMETERS PASSED
  path_data=paste0(dataset_path, dataset_type, "/")
  setwd(path_data)
  
  #USE DATASET NAME PARAMETER PASSED
  data=read_sas(paste0(dataset_name, ".", "sas7bdat"))

  title=title(program_id)
  footnotes = footnotes(program_id)
  
  data=filter_data(data, std_add_whr_clause)
  
  data=sort_data(data, std_sort_column)
  
  data=concat_data(data, std_concat)

#---> Perform page break before select data
  #data=page_break(data, std_page_break)
  
  data=rename_data(data, std_rename, std_concat)
  
#  data=select_data(data, std_cols)
  
  #Listing display
  #for display check the std_concat fields and ensure < for the field is next line
  
  doc=read_docx()
  doc=doc %>%
    body_default_section(landscape = TRUE)
  
  for (i in 1:length(title)){
    doc=doc %>% 
      body_add_par(title$Output_Description[i], style = "centered") # %>%
  }
  
  doc=doc%>% 
    body_add_table(data)
    
  for (i in 1:length(footnotes)){
    doc=doc %>% 
      body_add_par(footnotes$Value[i]) # %>%
  }
    
    # body_end_section(continuous = TRUE, landscape = TRUE) %>% 
    # 
    # body_add_par(value = "Table of tables", style = "heading 2") %>% 
    # body_add_toc(style = "table title") %>% 
    # body_add_par(value = "Table of graphics", style = "heading 2") %>% 
    # body_add_toc(style = "graphic title") # %>%
    print(doc, target = "~/georganh/Dynamic_Reporting/demo.docx")
}


  
  
  
  
  
  
  
  
  #listing_number=listing_number(output_number)
  
  #get population sub id dataset name, dataset type, field, filter symbol and 
  #filter condition
  pop_subid_dataset = pop_subid_dataset(std_popid)
  
  if (!pop_subid_dataset[2] %in% dataset_name) {
    path_data_subid=paste0(dataset_path, pop_subid_dataset[1], "/")
    setwd(path_data)
    data2 = read_sas(paste0(pop_subid_dataset[2], ".", "sas7bdat"))
    data2=filter_subid(data2, pop_subid_dataset[3], pop_subid_dataset[4], 
                       pop_subid_dataset[5])
  } else{
    data=filter_subid(data, pop_subid_dataset[3], pop_subid_dataset[4], 
                       pop_subid_dataset[5])
  }
      
  
}



ael001=function(indset,
              indset2,
              std_header,
              std_footer,
              vars){
  setwd(indset)
  data=read.csv(indset2, header = TRUE)
  
  std_header="hello"
                              
{ 
  
  #function to create demographics listing
  my_doc <- read_docx() ## creating blank document
  my_doc <- my_doc %>% 
    body_add_par("Listing Demographics ", style = "centered") %>% #Title 1
    body_add_par("Subject demographics", style = "centered") %>% #Title 2
    body_add_par("All Subjects", style = "centered") %>% #Title 3
    body_add_par("", style = "centered") %>% #Title 4
    body_add_par("", style = "centered") %>% #Title 5
    body_add_table(xx, style = "Normal Table") %>%  
    cursor_forward() %>%
    body_add_par(paste("TIME STAMP: ",Sys.time()), style = "toc 1") %>%
    body_end_section(landscape = TRUE)
  
  print(my_doc, target = "Listing Demographics.docx") 
}
