library(xlsx)
library(dplyr)
library(rlang)
library(tidyr)


concat_data = function(data, std_concat){
  
  param_data= read.xlsx("/home/georganh/georganh/Dynamic_Reporting/param_labels.xlsx", 
                        sheetIndex =1, stringsAsFactors=FALSE)
  
  conditions=unlist(strsplit(std_concat, ","))
  
  for (i in 1:length(conditions)){
    split_text=unlist(strsplit(conditions, "="))
    new_col_name = split_text[1]
    concat_col =as.vector(unlist(strsplit(split_text[2], "\\s+")))
    
    if (length(concat_col) == 1){
      str_sep = " / "
      concat_col =as.vector(unlist(strsplit(concat_col, "/")))
    } else {
      str_sep = "<"
    }
    
    data$new= paste(data[[concat_col[1]]], data[[concat_col[2]]], sep=str_sep)
    
    
    for (j in 3:length(concat_col)){
      data$new= paste(data$new, data[[concat_col[j]]], sep=str_sep) 
    }
    
    names(data)[names(data) == "new"] = new_col_name
  }
  return(data)
}

